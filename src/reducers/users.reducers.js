import {
    FETCH_USERS_PENDING,
    FETCH_USERS_ERROR,
    FETCH_USERS_SUCCESS,
    USERS_PAGINATION_CHANGE
} from "../contants/users.contants";

const initialState = {
    totalUser: 0,
    users: [],
    pending: false,
    error: null,
    currentPage: 1
}

export default function userReducers(state = initialState, action) {
    switch (action.type) {
        case FETCH_USERS_PENDING:
            state.pending = true;
            break;
        case FETCH_USERS_SUCCESS:
            state.totalUser = action.totalUser;
            state.pending = false;
            state.users = action.data;
            break;
        case FETCH_USERS_ERROR:
            break;
        case USERS_PAGINATION_CHANGE:
            state.currentPage = action.page;
            break;
        default:
            break;
    }

    return {...state};
}